# Button Shim library

## Features

- 5 Button
- Hold
- Press
- Release

## TODO

- LED driver

## Setup

```bash
$ git clone https://gitlab.com/jochen.keil/buttonshim
$ cd buttonshim
$ python3 -m venv env
$ source env/bin/activate
$ pip install build
```
