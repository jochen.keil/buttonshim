import smbus
import time
import logging
from enum import Enum, IntEnum
from threading import Thread, Event
from colorsys import hsv_to_rgb

logging.basicConfig(level=logging.DEBUG)

class Button(IntEnum):
  A = 0
  B = 1
  C = 2
  D = 3
  E = 4

class ButtonState(Enum):
  Hold = 0
  NoState = 1
  Pressed = 2
  Released = 3

class LongPress(Thread):
  def __init__(self, button_handler=[], button=0, ev_time_ns=0, long_press=0, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__abort = Event()
    self.__button = button
    self.__ev_time_ns = ev_time_ns
    self.__long_press = long_press
    self.__button_handler = button_handler

  def run(self):
    time.sleep(self.__long_press / 1E3)
    if not self.__abort.is_set():
      for handler in self.__button_handler[self.__button][ButtonState.Hold]:
        handler(self.__button, ButtonState.Hold, self.__ev_time_ns)

  def abort(self):
    self.__abort.set()

class ButtonShim(Thread):
  def __init__(self, init=True, long_press=1000, fps=60, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.__stop = Event()

    self.__fps = fps
    self.__long_press = long_press

    self.__press_time = \
        { Button.A: None
        , Button.B: None
        , Button.C: None
        , Button.D: None
        , Button.E: None
        }

    self.__long_press_threads = \
        { Button.A: LongPress()
        , Button.B: LongPress()
        , Button.C: LongPress()
        , Button.D: LongPress()
        , Button.E: LongPress()
        }

    self.__button_handler = \
        { Button.A: {ButtonState.Hold: [], ButtonState.Pressed: [], ButtonState.Released: []}
        , Button.B: {ButtonState.Hold: [], ButtonState.Pressed: [], ButtonState.Released: []}
        , Button.C: {ButtonState.Hold: [], ButtonState.Pressed: [], ButtonState.Released: []}
        , Button.D: {ButtonState.Hold: [], ButtonState.Pressed: [], ButtonState.Released: []}
        , Button.E: {ButtonState.Hold: [], ButtonState.Pressed: [], ButtonState.Released: []}
        }

    self.__last_states = 0b00011111

    self.__BUS = None
    self.__ADDR = 0x3f
    self.__NUM_BUTTONS = 5
    self.__ERROR_LIMIT = 10
    self.__REG_INPUT = 0x00
    self.__REG_OUTPUT = 0x01
    self.__REG_POLARITY = 0x02
    self.__REG_CONFIG = 0x03
    self.__LED_GAMMA = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2,
        2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5,
        6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10, 11, 11,
        11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18,
        19, 19, 20, 21, 21, 22, 22, 23, 23, 24, 25, 25, 26, 27, 27, 28,
        29, 29, 30, 31, 31, 32, 33, 34, 34, 35, 36, 37, 37, 38, 39, 40,
        40, 41, 42, 43, 44, 45, 46, 46, 47, 48, 49, 50, 51, 52, 53, 54,
        55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
        71, 72, 73, 74, 76, 77, 78, 79, 80, 81, 83, 84, 85, 86, 88, 89,
        90, 91, 93, 94, 95, 96, 98, 99, 100, 102, 103, 104, 106, 107, 109, 110,
        111, 113, 114, 116, 117, 119, 120, 121, 123, 124, 126, 128, 129, 131, 132, 134,
        135, 137, 138, 140, 142, 143, 145, 146, 148, 150, 151, 153, 155, 157, 158, 160,
        162, 163, 165, 167, 169, 170, 172, 174, 176, 178, 179, 181, 183, 185, 187, 189,
        191, 193, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214, 216, 218, 220,
        222, 224, 227, 229, 231, 233, 235, 237, 239, 241, 244, 246, 248, 250, 252, 255]

    if init:
      self.init()

  def __del__(self):
    self.stop()

  def init(self):
    if self.__BUS is not None:
      return

    self.__BUS = smbus.SMBus(1)

    self.__BUS.write_byte_data(self.__ADDR, self.__REG_CONFIG, 0b00011111)
    self.__BUS.write_byte_data(self.__ADDR, self.__REG_POLARITY, 0b00000000)
    self.__BUS.write_byte_data(self.__ADDR, self.__REG_OUTPUT, 0b00000000)

    # self.set_pixel(0, 0, 0)

  def stop(self):
    for t in self.__long_press_threads.values():
      t.abort()
    self.__stop.set()

  def __button_is_pressed(self, states, button):
    return bool((states >> button) & 1 ^ 1)

  def register(self, button: Button, state: ButtonState, handler):
    self.__button_handler[button][state].append(handler)

  def run(self):
    while not self.__stop.is_set():
      ev_time_ns = time.time_ns()
      states = self.__BUS.read_byte_data(self.__ADDR, self.__REG_INPUT)

      for button in Button:
        state = ButtonState.NoState
        press_duration = None
        is_pressed = self.__button_is_pressed(states, button)
        was_pressed = self.__button_is_pressed(self.__last_states, button)

        # button was pressed
        if is_pressed and not was_pressed:
          state = ButtonState.Pressed
          self.__press_time[button] = ev_time_ns
          lp_thread = LongPress(self.__button_handler, button, ev_time_ns, self.__long_press)
          lp_thread.start()
          self.__long_press_threads[button] = lp_thread

        # button was released
        elif was_pressed and not is_pressed:
          state = ButtonState.Released

          if self.__press_time[button] is not None:
            press_duration = ev_time_ns - self.__press_time[button]
            # button was pressed longer than $long_press
            if (press_duration / 1E6) > self.__long_press:
              # nothing else to do here, the long press thread is already running
              state = ButtonState.Hold

            # button was pressed shorter than $long_press
            else:
              self.__long_press_threads[button].abort()

          self.__press_time[button] = None


        if state == ButtonState.Pressed or state == ButtonState.Released:
          for handler in self.__button_handler[button][state]:
            handler(button, state, ev_time_ns, press_duration)

      self.__last_states = states
      time.sleep(1.0 / self.__fps)

#
#
#
#
# LED_DATA = 7
# LED_CLOCK = 6
#
# REG_INPUT = 0x00
#
# ERROR_LIMIT = 10
#
# FPS = 60
#
# # The LED is an APA102 driven via the i2c IO expander.
# # We must set and clear the Clock and Data pins
# # Each byte in _reg_queue represents a snapshot of the pin state
#
# _reg_queue = []
# _update_queue = []
# _brightness = 0.5
#
# _led_queue = queue.Queue()
#
# _t_poll = None
#
# _running = False
#
# _states = 0b00011111
#
#
# class Handler():
#     def __init__(self):
#         self.press = None
#         self.release = None
#
#         self.hold = None
#         self.hold_time = 0
#
#         self.repeat = False
#         self.repeat_time = 0
#
#         self.t_pressed = 0
#         self.t_repeat = 0
#         self.hold_fired = False
#
#
# _handlers = [Handler() for x in range(NUM_BUTTONS)]
#
#
# def _run():
#     global _running, _states
#     _running = True
#     _last_states = 0b00011111
#     _errors = 0
#
#     while _running:
#         led_data = None
#
#         try:
#             led_data = _led_queue.get(False)
#             _led_queue.task_done()
#
#         except queue.Empty:
#             pass
#
#         try:
#             if led_data:
#                 for chunk in _chunk(led_data, 32):
#                     _bus.write_i2c_block_data(ADDR, REG_OUTPUT, chunk)
#
#             _states = _bus.read_byte_data(ADDR, REG_INPUT)
#
#         except IOError:
#             _errors += 1
#             if _errors > ERROR_LIMIT:
#                 _running = False
#                 raise IOError("More than {} IO errors have occurred!".format(ERROR_LIMIT))
#
#         for x in range(NUM_BUTTONS):
#             last = (_last_states >> x) & 1
#             curr = (_states >> x) & 1
#             handler = _handlers[x]
#
#             # If last > curr then it's a transition from 1 to 0
#             # since the buttons are active low, that's a press event
#             if last > curr:
#                 handler.t_pressed = time.time()
#                 handler.hold_fired = False
#
#                 if callable(handler.press):
#                     handler.t_repeat = time.time()
#                     Thread(target=handler.press, args=(x, True)).start()
#
#                 continue
#
#             if last < curr and callable(handler.release):
#                 Thread(target=handler.release, args=(x, False)).start()
#                 continue
#
#             if curr == 0:
#                 if callable(handler.hold) and not handler.hold_fired and (time.time() - handler.t_pressed) > handler.hold_time:
#                     Thread(target=handler.hold, args=(x,)).start()
#                     handler.hold_fired = True
#
#                 if handler.repeat and callable(handler.press) and (time.time() - handler.t_repeat) > handler.repeat_time:
#                     _handlers[x].t_repeat = time.time()
#                     Thread(target=_handlers[x].press, args=(x, True)).start()
#
#         _last_states = _states
#
#         time.sleep(1.0 / FPS)
#
#
# def _set_bit(pin, value):
#     global _reg_queue
#
#     if value:
#         _reg_queue[-1] |= (1 << pin)
#     else:
#         _reg_queue[-1] &= ~(1 << pin)
#
#
# def _next():
#     global _reg_queue
#
#     if len(_reg_queue) == 0:
#         _reg_queue = [0b00000000]
#     else:
#         _reg_queue.append(_reg_queue[-1])
#
#
# def _enqueue():
#     global _reg_queue
#
#     _led_queue.put(_reg_queue)
#
#     _reg_queue = []
#
#
# def _chunk(l, n):
#     for i in range(0, len(l)+1, n):
#         yield l[i:i + n]
#
#
# def _write_byte(byte):
#     for x in range(8):
#         _next()
#         _set_bit(LED_CLOCK, 0)
#         _set_bit(LED_DATA, byte & 0b10000000)
#         _next()
#         _set_bit(LED_CLOCK, 1)
#         byte <<= 1
#
# def set_brightness(brightness):
#     global _brightness
#
#     setup()
#
#     if not isinstance(brightness, int) and not isinstance(brightness, float):
#         raise ValueError("Brightness should be an int or float")
#
#     if brightness < 0.0 or brightness > 1.0:
#         raise ValueError("Brightness should be between 0.0 and 1.0")
#
#     _brightness = brightness
#
#
# def set_pixel(r, g, b):
#     """Set the Button SHIM RGB pixel
#
#     Display an RGB colour on the Button SHIM pixel.
#
#     :param r: Amount of red, from 0 to 255
#     :param g: Amount of green, from 0 to 255
#     :param b: Amount of blue, from 0 to 255
#
#     You can use HTML colours directly with hexadecimal notation in Python. EG::
#
#         buttonshim.set_pixel(0xFF, 0x00, 0xFF)
#
#     """
#     setup()
#
#     if not isinstance(r, int) or r < 0 or r > 255:
#         raise ValueError("Argument r should be an int from 0 to 255")
#
#     if not isinstance(g, int) or g < 0 or g > 255:
#         raise ValueError("Argument g should be an int from 0 to 255")
#
#     if not isinstance(b, int) or b < 0 or b > 255:
#         raise ValueError("Argument b should be an int from 0 to 255")
#
#     r, g, b = [int(x * _brightness) for x in (r, g, b)]
#
#     _write_byte(0)
#     _write_byte(0)
#     _write_byte(0b11101111)
#     _write_byte(LED_GAMMA[b & 0xff])
#     _write_byte(LED_GAMMA[g & 0xff])
#     _write_byte(LED_GAMMA[r & 0xff])
#     _write_byte(0)
#     _write_byte(0)
#     _enqueue()
